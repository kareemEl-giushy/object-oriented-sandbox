<?php
	// to difine a class
	class user {
		// properties (attribute)
	
		public $name;
	
		// methods (functions)
		public function sayhello() {
			return $this->name . ' seid Hello';
		}
	}
	
	// instantiate a user object from the user class
	$user1 = new user();
	// set the name for this user
	$user1->name = 'kareem';
	// print out the user name from the calss
	echo $user1->name;
	
	echo "<br/>";
	
	// user the method that is in the class
	echo $user1->sayhello();

	echo "<hr/>";
	// create a new user
	$user2 = new user();
	// set the name for this user
	$user1->name = 'Ahmed';
	// print out the user name from the calss
	echo $user1->name;
	
	echo "<br/>";
	
	// user the method that is in the class
	echo $user1->sayhello();