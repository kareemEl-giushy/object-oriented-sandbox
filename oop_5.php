<?php
	
	class user {
		public $name;
		public $age;
		public static $minpasslen = 6;

		public static function checklen($pass) {
			if (strlen($pass) >= self::$minpasslen) {
				return true;
			}else {
				return false;
			}
		}
	}

	if (user::checklen(123)) {
		echo 'your password is valid<br>';
	}else {
		echo 'your password is invalid<br>';
	}

	echo user::$minpasslen . '<br>';

	$user = new user();

	echo $user::$minpasslen;