<?php
	
	class user{
		public $name;
		public $age;

		// the construct method starts in the begining of instantiating a class
		public function __construct($name, $age) {
			
			$this->name = $name;
			$this->age = $age;
		}

		public function sayhello() {
			return "Your Name Is :- (" . $this->name . ') And Your Age Is :- (' . $this->age . ')<br/>';
		}

		// runs when you finished using the class after instantiating
		// the last thing that run in the page
		public function __destruct() {
			echo "Destruct is runing";
		}
	}

	// instatiating the class and set the arguments (parameters) of the construct method
	$user = new user('Kareem', 16);

	// printing the outputs
	echo $user->name . "<br/>";

	echo $user->age . "<br/>";

	echo $user->sayhello();

	echo "<hr>";

	// create a new user
	$user2 = new user('Ahmed', 7);

	// printing the outputs
	echo $user2->name . "<br/>";

	echo $user2->age . "<br/>";

	echo $user2->sayhello();

	echo "<hr>";
