<?php
	class user {
		protected $name;
		protected $age;
		
		public function __construct($name, $age) {
			$this->name = $name;
			$this->age = $age;
			// echo 'construct...';
		}
	}

	class customer extends user {
		protected $balence;

		// the same result as the ubove construct
		public function __construct($name, $age, $balence) {
			parent::__construct($name, $age);
			$this->balence = $balence;
		}

		public function pay($amount) {
			return $this->name . ' Paied $' . $amount;
		}
		public function getbalnce() {
			return $this->balence;
		}

		// // you can put it in any Class
		// public function __set($property, $value){
		// 	if (property_exists($this, $property)) {
		// 		$this->$property = $value;
		// 	}
		// 	return $this;
		// }
	}

	$user = new customer('kareem', 16, 500);

	echo $user->pay(50) . "</br>";
	echo $user->getbalnce();
	// $user->__set('name', 'Hamada');
	// echo $user->pay(50) . "</br>";