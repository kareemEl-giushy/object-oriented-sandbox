<?php
	
	class user {
		// private means that you can only access these variables ==inside== the class (Only)
		private $name;

		// Protected means that you can access these variables inside the class and from any class 
		// that inherite from this class as well (extends from this class)
		protected $age;

		public function __construct($name, $age) {
			$this->name = $name;
			$this->age = $age;
			
		}

		public function getname() {
			return $this->name;
		}

		public function setname($name) {
			$this->name = $name;
		}

		//__get magic method to get all variables (from the language structure just like __construct)
		public function __get($property) {
			if ( property_exists($this, $property) ){
				return $this->$property;
			}
		}
		
		// try to make my magic function
		public function getty($property) {
			if (property_exists($this, $property)) {
				return $this->$property;
			}
		}

		// __set magic method you can make this function by yourself with any name
		public function __set($property, $value) {
			// syntax property_exists("className", "property");
			if (property_exists('user', $property)) {
				$this->$property = $value;
			}
			// to return the properties (variables) of the class AS AN ( OBJECT )
			// (optional)
			return $this;
		}
	}

	$user = new user('Kareem', 16);
	// $user->name = 'Ahmed'; // (error) because the name var is private (for the class only)

	// echo $name . $age; // (error) because the name var is private and the age var is protected
	// =========$age->(For the exact same class and any extends class)======

	// echo 'The original name is ' . $user->getname() . "<br><br>";

	// $user->setname('Ahmed');
	// echo "after modifying :- " . $user->getname() . "<br><br>";

	// get by magic method __get

	echo $user->__get("name") . '<br>';
	echo $user->__get("age") . '<br>';

	echo "<hr>";
	// my personal (get) magic funcion (it worked) :)
	echo $user->getty("name") . '<br>';
	echo $user->getty("age") . '<br>';

	// try the __set magic method
	$user->__set('name', 'Ahmed');
	echo $user->getty('name');
	// print_r( $user->__set('name', 'Hamada') );